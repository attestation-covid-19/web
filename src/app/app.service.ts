import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {finalize} from 'rxjs/operators';
import {CertificateCreateDto} from './app.interfaces';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public readonly loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private readonly url: string = environment.api.baseUrl;

  constructor(private readonly http: HttpClient) {}

  static downLoadFile(data: ArrayBuffer, fileName: string, type: string) {
    const blob = new Blob([data], {type});
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    link.click();
  }

  public generateCertificate(data: CertificateCreateDto): Observable<ArrayBuffer> {
    let subscription: Subscription;
    this.loading$.next(true);

    const observable = this.http.post(`${this.url}/certificate/generate`, data, {responseType: 'arraybuffer'}).pipe(
      finalize(() => {
        this.loading$.next(false);
        subscription.unsubscribe();
      }),
    );

    subscription = observable.subscribe((buffer) => {
      AppService.downLoadFile(buffer, 'Attestation_de_deplacement_derogatoire.pdf', 'application/pdf');
    });

    return observable;
  }
}
