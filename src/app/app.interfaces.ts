export enum Purpose {
  work = 'work',
  shopping = 'shopping',
  health = 'health',
  social = 'social',
  sport = 'sport',
  convocation = 'convocation',
  missions = 'missions',
}

export interface CertificateCreateDto {
  name: string;
  birthDate: string;
  birthCity: string;
  address: string;
  city: string;
  zipCode: string;
  reason: Purpose;
  creationLocation: string;
  creationDate: string;
  creationTime: string;
  signature: string;
}
