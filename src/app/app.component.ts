import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, fromEvent, Observable} from 'rxjs';
import {pairwise, switchMap, takeUntil} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Purpose} from './app.interfaces';
import {AppService} from './app.service';
import * as moment from 'moment-timezone';
import {Moment} from 'moment-timezone/moment-timezone';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('signature') private signature: ElementRef;
  private cx: CanvasRenderingContext2D;
  private currentDate: Date = new Date();
  private currentUtcFormattedDate: Moment = moment.tz(moment().format('YYYY-MM-DDTHH:mm:ss'), 'UTC').local();
  public readonly purposes = [
    { translateKey: 'form.purposes.work', value: Purpose.work },
    { translateKey: 'form.purposes.shopping', value: Purpose.shopping },
    { translateKey: 'form.purposes.health', value: Purpose.health },
    { translateKey: 'form.purposes.social', value: Purpose.social },
    { translateKey: 'form.purposes.sport', value: Purpose.sport },
    { translateKey: 'form.purposes.convocation', value: Purpose.convocation },
    { translateKey: 'form.purposes.missions', value: Purpose.missions },
  ];
  public loading$: Observable<boolean>;
  public submitted$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public startDate: Date = new Date();
  public name: FormControl = new FormControl(undefined, Validators.required);
  public birthDate: FormControl = new FormControl(undefined, Validators.required);
  public birthCity: FormControl = new FormControl(undefined, Validators.required);
  public address: FormControl = new FormControl(undefined, Validators.required);
  public city: FormControl = new FormControl(undefined, Validators.required);
  public zipCode: FormControl = new FormControl(undefined, [Validators.required, Validators.minLength(5), Validators.maxLength(5)]);
  public reason: FormControl = new FormControl(undefined, Validators.required);
  public creationLocation: FormControl = new FormControl(undefined, Validators.required);
  public creationDate: FormControl = new FormControl(this.currentUtcFormattedDate.format(), Validators.required);
  public creationTime: FormControl = new FormControl(moment().format('HH:mm'), Validators.required);
  public signatureControl: FormControl = new FormControl(undefined);
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder, private appService: AppService) {}

  ngOnInit(): void {
    this.loading$ = this.appService.loading$;
    this.startDate.setFullYear(this.currentDate.getFullYear() - 30);
    this.form = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return this.formBuilder.group({
      name: this.name,
      birthDate: this.birthDate,
      birthCity: this.birthCity,
      address: this.address,
      city: this.city,
      zipCode: this.zipCode,
      reason: this.reason,
      creationLocation: this.creationLocation,
      creationDate: this.creationDate,
      creationTime: this.creationTime,
      signature: this.signatureControl,
    });
  }

  async submit() {
    this.submitted$.next(true);

    if (this.form.invalid) {
      return;
    }

    this.appService.generateCertificate(this.form.value);
  }

  ngAfterViewInit(): void {
    const canvasEl: HTMLCanvasElement = this.signature.nativeElement;
    canvasEl.height = 300;
    canvasEl.width = 400;

    this.cx = canvasEl.getContext('2d');
    this.cx.lineWidth = 3;
    this.cx.lineCap = 'round';
    this.cx.strokeStyle = '#000';

    this.captureMouseEvents(canvasEl);
    this.captureTouchEvents(canvasEl);
  }

  clearSignature(event: Event) {
    const canvasEl: HTMLCanvasElement = this.signature.nativeElement;
    this.cx.clearRect(0, 0, canvasEl.width, canvasEl.height);
    this.signatureControl.setValue(canvasEl.toDataURL());
    event.preventDefault();
  }

  private captureTouchEvents(canvasEl: HTMLCanvasElement) {
    fromEvent(canvasEl, 'touchstart')
      .pipe(
        switchMap(() => fromEvent(canvasEl, 'touchmove')
          .pipe(
            takeUntil(fromEvent(canvasEl, 'touchend')),
            takeUntil(fromEvent(canvasEl, 'touchcancel')),
            pairwise(),
          )
        )
      )
      .subscribe((res: [TouchEvent, TouchEvent]) => {
        const rect = canvasEl.getBoundingClientRect();
        res[0].preventDefault();
        res[1].preventDefault();

        const prevPos = {
          x: res[0].touches[0].clientX - rect.left,
          y: res[0].touches[0].clientY - rect.top
        };

        const currentPos = {
          x: res[1].touches[0].clientX - rect.left,
          y: res[1].touches[0].clientY - rect.top
        };

        this.drawOnCanvas(prevPos, currentPos);
        this.signatureControl.setValue(canvasEl.toDataURL());
      });
  }

  private captureMouseEvents(canvasEl: HTMLCanvasElement) {
    fromEvent(canvasEl, 'mousedown')
      .pipe(
        switchMap(() => fromEvent(canvasEl, 'mousemove')
          .pipe(
            takeUntil(fromEvent(canvasEl, 'mouseup')),
            takeUntil(fromEvent(canvasEl, 'mouseleave')),
            pairwise(),
          )
        )
      )
      .subscribe((res: [MouseEvent, MouseEvent]) => {
        const rect = canvasEl.getBoundingClientRect();

        const prevPos = {
          x: res[0].clientX - rect.left,
          y: res[0].clientY - rect.top
        };

        const currentPos = {
          x: res[1].clientX - rect.left,
          y: res[1].clientY - rect.top
        };

        this.drawOnCanvas(prevPos, currentPos);
        this.signatureControl.setValue(canvasEl.toDataURL());
    });
  }

  private drawOnCanvas(prevPos: { x: number, y: number }, currentPos: { x: number, y: number }) {
    if (!this.cx) { return; }

    this.cx.beginPath();

    if (prevPos) {
      this.cx.moveTo(prevPos.x, prevPos.y);
      this.cx.lineTo(currentPos.x, currentPos.y);
      this.cx.stroke();
    }
  }
}
