export const environment = {
  production: true,
  api: {
    baseUrl: 'https://europe-west1-attestation-covid-19.cloudfunctions.net/api'
  }
};
